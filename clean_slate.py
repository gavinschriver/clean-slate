from selenium import webdriver
import re
import csv
from webdriver_manager.chrome import ChromeDriverManager
from dotenv import load_dotenv
from os import getenv
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException


options = Options()
if getenv("CLEAN_SLATE_DONT_WAIT_FOR_CAPTCHA"):
    options.headless = True

load_dotenv()





site = 'https://cjs.shelbycountytn.gov/CJS/Account/Login'
driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)

name_or_case_number = 'smith, john'
date_of_birth = "11/07/1981"

if getenv("CLEAN_SLATE_USERNAME"):
    username = getenv("CLEAN_SLATE_USERNAME")
else:
    username = input('Please enter your user name to log in: ')
if getenv("CLEAN_SLATE_PASSWORD"):
    password = getenv("CLEAN_SLATE_PASSWORD")
else:
    password = input('As well as your password: ')



def login_portal(username, password):
    
    driver.get(site)
    
    user_name_entry = WebDriverWait(driver, 30).until(
    EC.presence_of_element_located((By.ID, 'UserName')))

    ## User Info entry and click login
    user_name_entry.send_keys(username)
    password_entry = driver.find_element_by_id('Password')
    password_entry.send_keys(password)
    login_click = driver.find_element_by_css_selector('.btn').click()
    
    
    #login_confirmation_wait = WebDriverWait(driver, 30).until(
    #EC.presence_of_element_located((By.CLASS_NAME, 'btn-description')))
    search_site = 'https://cjs.shelbycountytn.gov/CJS/Home/Dashboard/29'
    driver.get(search_site)


## Enter search of site
def search_and_expunge():

    record_search = WebDriverWait(driver, 30).until(
    EC.presence_of_element_located((By.ID, 'caseCriteria_SearchCriteria')))
    
    record_search.send_keys(name_or_case_number)
    click_submit = driver.find_element_by_id("btnSSSubmit").click()


    ## Entering in DOB    
    dob_drop_down = WebDriverWait(driver, 30).until(
    EC.presence_of_element_located((By.XPATH, "//th[@data-field='DateOfBirthSort']//span[@title='Sort / Filter Options']"))).click()    
    
    dob_text_box = WebDriverWait(driver, 30).until(
    EC.presence_of_element_located((By.XPATH, "//div[@class='k-animation-container']//input[@class='k-input']")))
    ##For some reason, if I get rid of this .click() below and placed it above like the others, I get
    ##AttributeError: 'NoneType' object has no attribute 'send_keys'    
    dob_text_box.click()
    
    dob_text_box.send_keys(date_of_birth)
    dob_submit = driver.find_element_by_xpath("//div[@class='k-animation-container']//button[@type='submit']").click()



    ##From name search results (after DOB is entered), gets to info to be downloaded
    ##From here I can find all the caseLinks and store them in a list, and then loop thru each.
    ##Each going click case# (@detailed), scrape info, press back, find next case#, repeat
    




## Here for multiple case numbers
def case_numbers():
    #case_numbers_wait = WebDriverWait(driver, 30).until(
    #EC.presence_of_element_located((By.CLASS_NAME, 'caseLink' )))
    
    case_links = driver.find_elements_by_class_name('caseLink')

    #save a shot of the page so we know what it looks like after trying to entery the DOB
    page = driver.find_element_by_tag_name("body")
    page.screenshot("results/case_search_results_page.png")

    #save the page HTML for further investigation 
    content = driver.page_source

    with open('results/case_results_page.html', 'w') as f:

        f.write(content)
    
    if len(case_links) == 0:
        print('No cases matching this DOB')


    with open('results/scratch_slate_csv.csv', 'w', newline = '') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['Name', "DoB", "Case Number", "Disp Date", 'Charge', "Result", ])


            
    for i in range(len(case_links)):
        case_links[i].click()
        
    ##Here is where the downloading starts
        scrape_case_number = WebDriverWait(driver, 30).until(
        EC.presence_of_element_located((By.XPATH, "//div[@id='divCaseInformation_body']//span[contains(text(), 'Case Number')]/parent::*")))
                                        
        disp_number = driver.find_elements_by_id('DispositionEventsPrintSection')
        disp_rows = driver.find_elements_by_xpath("//*[contains(@id, 'CriminalDispositions')]/table/tbody/tr")
        disp_cells = driver.find_elements_by_xpath("//*[contains(@id, 'CriminalDispositions')]/table/tbody/tr/td")

        disp_date_regex = re.compile(r'(\d\d.\d\d.\d\d\d\d) Disposition')
        

        a = 1
        b = 2

        if len(disp_number) > 0:
            for i in range(len(disp_rows)):
                disp_date_match = disp_date_regex.search(disp_number[0].text) #This is here because it throws a fit when there are no dispo events (if in an active/open case).
                
                with open('results/scratch_slate_csv.csv', 'a', newline = '') as csvfile:
                    writer = csv.writer(csvfile)
                    writer.writerow([name_or_case_number, date_of_birth, scrape_case_number.text, disp_date_match.group(1), disp_cells[a].text, disp_cells[b].text])
                    a += 3
                    b += 3
                
        else:
            with open('scratch_slate_csv.csv', 'a', newline = '') as csvfile:
                    writer = csv.writer(csvfile)
                    writer.writerow([name_or_case_number, date_of_birth, "No Dispo info found"])




                
            
    ##This goes back to search results
        back_to_search = driver.find_element_by_xpath("//p[contains(text( ), 'Search Results')]").click()




login_portal(username, password)


try:
    driver.find_element_by_class_name('g-recaptcha')
    title_text = driver.find_element_by_xpath('/html/head/title').get_attribute('textContent')
    print(f"captcha detected on page: {title_text}")
    if getenv("CLEAN_SLATE_DONT_WAIT_FOR_CAPTCHA"):
        print("captcha detected in environment where not exptected; terminating script")
        quit()
except NoSuchElementException:
    print('no captcha')

if not getenv("CLEAN_SLATE_DONT_WAIT_FOR_CAPTCHA"):
    input("Hit enter when you've completed the captcha.")
else:
    print("If you get a Captcha, configure your run differently to wait for it.")

search_and_expunge()
case_numbers()


#before I forget, it'd be cool if there's no captcha to not need to hit enter







## So I want to look insdie the DispoPrint Selection, find which headers contain "disposition", and then get each tr that under the repective heads
## TR's contain each line that I'm looking for
## [table] role="table"/Tbody/tr/td(can skip 1st td[just a number])
## This is giving a start, works with erry page so far. Need to refine. dispo_test = driver.find_elements_by_xpath("//*[contains(@id, 'CriminalDispositions')]/table/tbody/tr/td")


##So, i can run a command to see how many tr's there are, and that's how many new lines will be added to the csv.
##There's gotta be an easy way to designate which td's will go where. Let's go simple fn.
## I'll need nested for loop. For each cacse number will have multiple tr's (get length of tr's) for each charge [charge=tr?]


##prob is too big rn. I've got case#, DOB, name, dspoDate, just need Charges and Finding.
##ok, one loop per dispo_row
##Then just place 1 and 2 in the remaining spots.
##But I need to make sure that it's going thru the whole list, not just rep'ing


##
##I want to pressure test this
## I can test to make sure this works by removing the dob and therefore hitting multiple people.




## Also looks like one person can have more than 10 cases and that makes a new page, gotta check for that. Check if person has >10 cases, if so click button to change case list
## Looks like some ppl don't have dispo events, line 92 gets tripped. If statement out.
##      This one^^ has an "H" in the case#, also says "active or open" in status
## Might be a smarter/more effective idea to search the status and see if it's "Active or Open", if so, abort
